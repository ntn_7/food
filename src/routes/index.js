import React from "react";
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from '../component/Home/index'

const AppContainer = createAppContainer(App);

const App = createStackNavigator({

    Home: Home,
  }, {
    initialRouteName: 'Home',
    transitionConfig,
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  })

export default class Routes extends React.Component {

    render() {
      return <AppContainer />
    }
  }