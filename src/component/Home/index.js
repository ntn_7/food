import React, { Component } from "react";
import {
    StatusBar, View, Text, Modal, TouchableOpacity
} from 'react-native';
import HomeModal from "../../common/HomeModal";

const mainArray = []
const content = []

export default class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            openModal: false,
            dataArray: []
        }
    }

    componentDidMount() {
        this.getData()
    }

    getData = async () => {

        await fetch(`https://api.jsonbin.io/b/5f2c36626f8e4e3faf2cb42e`, {
            method: 'GET'

        }).then((response) => response.json())
            .then((responseJson) => {

                // console.log("Res", responseJson)
                const data = responseJson['categories']
                let title = []
                let content = []
                // let newArray = []
                for (let i = 0; i < data.length; i++) {
                    // console.log("Title", data[i]['category']['categoryName'])
                    // title[i] = data[i]['category']['categoryName']
                    // content[i] = data[i]['category']['subcategories'][0]['items']
                    // newArray.push('title',data[i]['category']['categoryName'])
                    mainArray.push({ 'title': data[i]['category']['categoryName'], 'content': data[i]['category']['subcategories'][0]['items'] })
                    content.push(data[i]['category']['subcategories'][0]['items'])
                    // mainArray.push({'content' : data[i]['category']['subcategories'][0]['items']})
                }
                console.log("content array", content)
                // console.log("Title",title)
                // const title = responseJson['categories'][0]['category']['categoryName']
                console.log("Res1", data)
                console.log("content", content)
                this.setState({ dataArray: data })

                // if (responseJson.status === "Failure") {
                //   this.setState({ loading: false })
                // }
                // else {
                //   const data = responseJson['data']
                //   console.warn('my respkn e ' + data)
                //   this.setState({ loading: false, Alert_Visibility: true, dataArray: data })
                // }

            }).catch((error) => {
                console.error(error);
            })
    }

    // _renderContent = () => {
    //     return (
    //         <View style={{ length: 100, width: 50, margin: 15 }}>
    //             <Text>{content[1]}</Text>
    //         </View>
    //     )
    // }

    openHomeModal = (status, mainArray) => {
        this.refs.homeModal.modalHandler(status, mainArray)
        }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <TouchableOpacity
                    onPress={this.openHomeModal(true, mainArray)}
                    style={{ alignSelf: "center", width: 200, height: 50, backgroundColor: "blue", borderRadius: 10, marginVertical: 300 }}>
                    <Text style={{ fontSize: 20, color: "#fff", alignSelf: "center", marginTop: 8 }}>Push Me</Text>
                </TouchableOpacity>

                <HomeModal
                   ref = "homeModal"
                />

            </View>
        )
    }

}