import React, { Component } from 'react'
import {
    Modal, View, Text, TouchableOpacity
} from 'react-native';
import { Accordion, Content } from "native-base";

const data = []

export default class HomeModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            openModal: false
        }
    }

    modalHandler = (status, mainArray) => {
        this.setState({
            openModal: status
        })

        data = mainArray
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Modal
                    visible={this.state.openModal}
                    onRequestClose={() => this.setState({ openModal: false })}
                >
                    <Text style={{ margin: 40, fontSize: 20, fontWeight: "bold" }}>Approved Food List</Text>
                    <Accordion
                        expanded={0}
                        dataArray={data} icon="add" expandedIcon="remove"
                        headerStyle={{ backgroundColor: "white", color: '#1c4478' }}
                        contentStyle={{ backgroundColor: "#f2f2f2", color: '#1c4478', padding: 5 }}
                    // renderContent={this._renderContent}
                    />
                </Modal>
            </View>
        );
    }


}